package cn.chowa.ejyy.fitment;

import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.*;
import cn.chowa.ejyy.model.entity.Fitment;
import cn.chowa.ejyy.model.entity.NoticeToUser;
import cn.chowa.ejyy.model.entity.NoticeTpl;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.FITMENT_CREATE_FAIL;
import static cn.chowa.ejyy.common.Constants.fitment.USER_SUBMIT_APPLY_STEP;

@RestController("fitmentCreate")
@RequestMapping("/pc/fitment")
public class create {

    @Autowired
    private FitmentRepository fitmentRepository;

    @Autowired
    private FitmentQuery fitmentQuery;

    /**
     * 装修登记
     */
    @SaCheckRole(Constants.RoleName.ZXDJ)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> create(@RequestBody RequestData data) {
        int building_id = data.getInt("building_id", true, "^\\d+$");
        long wechat_mp_user_id = data.getLong("wechat_mp_user_id", true, "^\\d+$");

        long unfinished = fitmentQuery.getUnfinishedFitmentCount(data.getCommunityId(), building_id);
        if (unfinished > 0) {
            throw new CodeException(FITMENT_CREATE_FAIL, "已有业主提交装修报备，请勿重复提交");
        }

        Fitment fitment = fitmentRepository.save(Fitment.builder()
                .communityId(data.getCommunityId())
                .buildingId(building_id)
                .step(USER_SUBMIT_APPLY_STEP)
                .wechatMpUserId(wechat_mp_user_id)
                .created_at(System.currentTimeMillis())
                .build());

        return Map.of("id", fitment.getId());
    }

}
