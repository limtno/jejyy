package cn.chowa.ejyy.epidemic;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.EpidemicRepository;
import cn.chowa.ejyy.model.entity.Epidemic;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;

@Slf4j
@RestController
@RequestMapping("/pc/epidemic")
public class create {

    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private EpidemicRepository epidemicRepository;

    @SaCheckRole(Constants.RoleName.YQFK)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> doCreate(@RequestBody RequestData data) {
        long wechat_mp_user_id = data.getLong("wechat_mp_user_id", true, "^\\d+$");
        long building_id = data.getLong("building_id", true, "^\\d+$");
        long communityId = data.getCommunityId();
        float temperature = data.getFloat("temperature", true, "^\\d+(\\.\\d+)?$");
        int tour_code = data.getInt("tour_code", true, "^1|2|3$");
        int return_hometown = data.getInt("return_hometown", true, "^1|0$");
        String return_from_province = data.getStr("return_from_province");
        String return_from_city = data.getStr("return_from_city");
        String return_from_district = data.getStr("return_from_district");

        ObjData verify = buildingQuery.getUserBuildingInfo(wechat_mp_user_id, building_id, communityId);
        if (verify == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法数据，不能作为疫情防控数据");
        }

        long id = epidemicRepository.save(Epidemic.builder()
                .wechatMpUserId(wechat_mp_user_id)
                .buildingId(building_id)
                .communityId(communityId)
                .tourCode(tour_code)
                .temperature(temperature)
                .returnHometown(return_hometown)
                .returnFromProvince(return_from_province)
                .returnFromCity(return_from_city)
                .returnFromDistrict(return_from_district)
                .createdBy(AuthUtil.getUid())
                .createdAt(System.currentTimeMillis())
                .build()).getId();

        return Map.of("id", id);
    }

}
