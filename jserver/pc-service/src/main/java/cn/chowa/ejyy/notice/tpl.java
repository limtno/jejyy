package cn.chowa.ejyy.notice;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController("noticeTpl")
@RequestMapping("/pc/notice")
public class tpl {

    @SaCheckRole(Constants.RoleName.ANYONE)
    @GetMapping("/tpl")
    public ObjData getTpl() {
        ObjData objData = new ObjData();
        objData.put("list", List.of(
                Map.of(
                        "title", "停水通知",
                        "tpl", "STOP_WATER",
                        "content", JSONUtil.parse("[\n" +
                                "    {\n" +
                                "        label: '',\n" +
                                "        key: 'first',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停水时间',\n" +
                                "        key: 'keyword1',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停水区域',\n" +
                                "        key: 'keyword2',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停水原因',\n" +
                                "        key: 'keyword3',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '',\n" +
                                "        key: 'remark',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    }\n" +
                                "]")
                ),
                Map.of(
                        "title", "停电通知",
                        "tpl", "STOP_ELECTRICITY",
                        "content", JSONUtil.parse("[\n" +
                                "    {\n" +
                                "        label: '',\n" +
                                "        key: 'first',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停电时间',\n" +
                                "        key: 'keyword1',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停电区域',\n" +
                                "        key: 'keyword2',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '停电原因',\n" +
                                "        key: 'keyword3',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    },\n" +
                                "    {\n" +
                                "        label: '',\n" +
                                "        key: 'remark',\n" +
                                "        type: 'DATA',\n" +
                                "        value: undefined\n" +
                                "    }\n" +
                                "]")
                )
        ));
        return objData;
    }

}
