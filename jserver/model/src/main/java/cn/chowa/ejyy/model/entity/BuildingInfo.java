package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * 房产信息
 */
@Data
@Entity
@Table(name = "ejyy_building_info")
public class BuildingInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty("community_id")
    private long communityId;

    /**
     * 1 住宅 ；2 车位； 3仓房; 4 商户；5 车库
     */
    private int type;

    /**
     * 单位后台控制 如 区 期
     */
    private String area;

    /**
     * 只有type为1时生效
     */
    private String building;

    /**
     * 单元号
     */
    private String unit;

    /**
     * 门牌号
     */
    private String number;

    @JsonProperty("construction_area")
    private float constructionArea;

    @JsonProperty("created_by")
    private long createdBy;

    @JsonProperty("created_at")
    private long createdAt;

}
