package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface RepairQuery {

    List<ObjData> getBuildingRepair(int repair_type, long community_id, int step, String refer, int page_size, int page_num);

    long getBuildingRepairCount(int repair_type, long community_id, int step, String refer);

    ObjData getBuildingRepairInfo(long id,long community_id);

}
