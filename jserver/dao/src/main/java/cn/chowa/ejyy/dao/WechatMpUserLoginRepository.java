package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.WechatMpUserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatMpUserLoginRepository extends JpaRepository<WechatMpUserLogin, Long> {
}
