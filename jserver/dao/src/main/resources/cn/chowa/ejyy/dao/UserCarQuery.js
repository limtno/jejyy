function getUserCars(community_id, is_new_energy, car_number, car_type, status, sync, page_size, page_num){
    var sql=`
        select
            a.id,
            a.car_number,
            a.car_type,
            a.is_new_energy,
            a.status,
            a.sync,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number
        from ejyy_user_car a left join ejyy_building_info b on a.building_id=b.id
        where b.community_id=:community_id
        ${is_new_energy?' and is_new_energy=:is_new_energy':''}
        ${car_number?' and car_number=:car_number':''}
        ${car_type?' and car_type=:car_type':''}
        ${status?' and status=:status':''}
        ${sync?' and sync=:sync':''}
         limit ${(page_num-1)*page_size},${page_size}
    `;
    return sql;
}

function getUserCarsCount( community_id, is_new_energy, car_number, car_type, status, sync){
    var sql=`
        select
           count(*)
        from ejyy_user_car a left join ejyy_building_info b on a.building_id=b.id
        where b.community_id=:community_id
        ${is_new_energy?' and is_new_energy=:is_new_energy':''}
        ${car_number?' and car_number=:car_number':''}
        ${car_type?' and car_type=:car_type':''}
        ${status?' and status=:status':''}
        ${sync?' and sync=:sync':''}
    `;
    return sql;
}

function getUserCarInfo( id, community_id){
    var sql=`
        select
            a.id,
            a.wechat_mp_user_id,
            c.real_name,
            a.building_id,
            a.car_number,
            a.car_type,
            a.is_new_energy,
            a.status,
            a.sync,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number
        from ejyy_user_car a left join ejyy_building_info b on a.building_id=b.id
            left join ejyy_wechat_mp_user c on a.wechat_mp_user_id=c.id
        where b.community_id=:community_id and a.id=:id
    `;
    return sql;
}

function getUserCarOperateLogs(id){
    var sql=`
        select
            a.status,
            a.operate_by,
            a.created_at,
            b.id as ejyy_wechat_mp_user_id,
            b.real_name as ejyy_wechat_mp_user_real_name,
            c.id as property_company_user_id,
            c.real_name as property_company_user_real_name
        from ejyy_user_car_operate_log a left join ejyy_wechat_mp_user b on a.wechat_mp_user_id=b.id
            left join ejyy_property_company_user c on a.property_company_user_id=c.id
        where a.user_car_id=:id
        order by a.id desc
    `;

    return sql;
}
