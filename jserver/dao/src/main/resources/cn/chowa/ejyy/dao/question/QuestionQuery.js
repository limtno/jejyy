// 获取问卷列表
function getQuestionnaireList(published, community_id, page_size, page_num){
    var where="1=1 and ";
    where+=published?"published=:published and ":"";

    var sql=`
        select
            a.*
        from ejyy_questionnaire a 
        where ${where} a.community_id=:community_id
        order by a.id desc
        limit ${(page_num-1)*page_size},${page_size}
    `;

    return sql;
}

function getQuestionnaireListCount(published, community_id){
    var where="1=1 and ";
    where+=published?"published=:published and ":"";

    var sql=`
        select
            count(*)
        from ejyy_questionnaire a 
        where ${where} a.community_id=:community_id
    `;

    return sql;
}

function getQuestioninfoById(published, community_id, page_size, page_num){
    var where="1=1 and ";
    where+=published?"published=:published and ":"";

    var sql=`
        select
            a.*
        from ejyy_questionnaire a 
        where ${where} a.community_id=:community_id
        order by a.id desc
        limit ${(page_num-1)*page_size},${page_size}
    `;

    return sql;
}